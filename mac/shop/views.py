from django.shortcuts import render
from django.http import HttpResponse
from .models import Products, Contact
from math import ceil

# Create your views here.
def index(request):
    '''products = Products.objects.all()
    print(products)
    n = len(products)
    nSlides = n//4 + ceil((n/4) - (n//4))
    #params = {"no_of_slide":nSlides, 'range':range(1,nSlides),"product":products}
    allProds = [[products, range(1, nSlides), nSlides],
                [products, range(1, nSlides), nSlides]]'''
    allProds = []
    catprods = Products.objects.values('category', 'id')
    cats = {item['category'] for item in catprods}
    for cat in cats:
        prod = Products.objects.filter(category=cat)
        n = len(prod)
        nSlides = n//4 + ceil((n/4) - (n//4))
        allProds.append([prod, range(1,nSlides), nSlides])
    params = {'allProds' : allProds}
    return render(request, 'shop/index.html', params)

def about(request):
    return render(request, 'shop/about.html')

def contact(request):
    if request.method == 'POST':
        name = request.POST.get("name", "")
        email = request.POST.get("email", "")
        phone = request.POST.get("phone", "")
        msg = request.POST.get("msg", "")

        contact = Contact(name=name, email=email,phone=phone,msg=msg)
        contact.save()

    return render(request, 'shop/contact.html')

def tracker(request):
    return render(request, 'shop/tracker.html')

def search(request):
    return HttpResponse("search Us")

def productView(request, myid):
    #Fetch the product by id
    product = Products.objects.filter(id=myid)
    #print(product)

    return render(request, 'shop/productView.html', {'product':product[0]})

def checkout(request):
    return render(request,'shop/checkout.html')
